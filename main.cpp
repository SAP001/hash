#include <iostream>
#include <cstring>
using namespace std;
struct Node
{
    char key[20];
    char value[20];
    Node * next;
    Node(): next{nullptr}{};
};

class hashTable
{
public:
    hashTable();
    void push( char * , const char * value );
    void pop( char * key );
    char * get( char * key );
private:
    int getIndex( char * );
    Node ** buffer;
};

hashTable::hashTable()
{
    buffer = new Node*[1000];
}

char* hashTable::get( char * key1 )
{
    int key = getIndex( key1 );
    if( !buffer[key] )
        return "none";

    Node * curr = buffer[key];
    while ( curr->next && strcmp( curr->key, key1 ) )
        curr = curr->next;
    if( !strcmp( curr->key, key1 ) )
        return curr->value;
    else
        return "none";
}

int hashTable::getIndex( char * str )
{
    int hash = 0;
    int a = 7;
        for( ; *str != 0; ++str )
            hash = ( hash * a + *str ) % 1000;
        return hash;
}

void hashTable::push( char * key, const char * value)
{
    int index = getIndex( key );

    if( buffer[index] == nullptr )
    {
        Node * tmp;
        Node * newNode = new Node;
        strcpy( newNode->key, key );
        strcpy( newNode->value, value );
        buffer[index] = newNode;
        return;
    }

    Node * searcher = buffer[index];
    while ( searcher->next && !strcmp(searcher->next->value, value) )
        searcher = searcher->next;

    if( !strcmp( searcher->key, key ) )
    {
        strcpy( searcher->value, value);
        return;
    }

    Node * tmp;
    Node * newNode = new Node;
    strcpy( newNode->value, value );
    searcher->next = newNode;
}


void hashTable::pop( char * key1 )
{
    int key = getIndex( key1 );

    if( buffer[key] == nullptr )
        return;

    Node * curr = buffer[key];
    Node * prev = nullptr;
    while ( curr->next && !strcmp( curr->key, key1 ) )
    {
        prev = curr;
        curr = curr->next;
    }

    if( !strcmp( curr->key, key1 ) )
    {
        if (prev) {
            prev->next = curr->next;
            delete curr;
        } else
        {
            buffer[key] = curr->next;
            delete curr;
        }
    }

}
using namespace std;
int main()
{
    hashTable * tt = new hashTable;
    tt->push("hello", "privet");
    tt->push("bye", "poka");
    cout << tt->get( "hello" );
    cout << tt->get( "bye" );
    tt->pop("hello");
    cout << tt->get("hello");
//    cout << tt->exist(2);
//    cout << tt->exist(4);
//    tt->push(2);а
//    tt->pop(2);
//    cout << tt->exist(2);

    return 0;
}



//int main() {
//    std::cout << "Hello, World!" << std::endl;
//    return 0;
//    int Hash( const char* str, int m )
//    {
//        int hash = 0;
//        for( ; *str != 0; ++str )
//            hash = ( hash * a + *str ) % m;
//        return hash;}
//}
